/*
 * gics.h
 *
 *  Created on: 2 mai 2018
 *      Author: GICS
 */

#ifndef GICS_H_
#define GICS_H_

#include "lwip/udp.h"
#include "main.h"
#include "stm32f4xx_hal.h"

#define RACK_ID 11
#define GICS_ID 4











typedef struct GICSOldTransaction {
	short function;
	short address;
	short count;
	short data[255];
} GICSOldTransaction;



typedef struct GICSTransaction {
	unsigned char function;
	unsigned char  magic;
	unsigned short length;
	unsigned short data[255];
} GICSTransaction;

enum GICS_MODE { GICS_MODE_REMOTE=0, GICS_MODE_LOCAL=1};
enum GICS_MODE gics_mode;

#define GICS_API 0
#define GICS_IED 1


#define GICS_MAGIC 		0xd0
#define GICS_READ		0x01
#define GICS_WRITE		0x02
#define GICS_DISCRETE	0x04
#define GICS_ANALOG		0x08
#define GICS_DA			0x10
#define GICS_READ_HOLDS 0x20
#define GICS_PARAMS     0x80


void gics_udp_server_receive_callback(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port);
void gics_udp_server_init(void);
#define GICS_UDP_SERVER_PORT    2015   /* define the UDP local connection port */
#define GICS_UDP_CLIENT_PORT    2015   /* define the UDP remote connection port */
void READ_MCP(unsigned char Addr, uint8_t cmdByte, uint8_t * pBuffer);
void WRITE_MCP(unsigned char Addr, uint8_t cmdByte, uint8_t LSBdata, uint8_t MSBdata);

//Stockage des valeurs des voies de l'ADC
#define N_ADC_CHANNELS 9
__IO uint16_t ADCxDATA[N_ADC_CHANNELS]; //halfword -> donnees sur 16 bits

//Contr�le des DAC
#define N_DAC_CHANNELS 8
#define DACFREQUENCY 10000
#define SINUSFREQUENCY 50
#define _50HzPHASEINC (SINUSFREQUENCY*65535/DACFREQUENCY)
__IO uint16_t PHASEINC;

__IO uint16_t VDAC_CTRL[N_DAC_CHANNELS];
__IO uint16_t PHASEOFFSET[N_DAC_CHANNELS];
__IO float 	 LTC1660LookupTable[1024];
__IO float 	 AMPLITUDE[N_DAC_CHANNELS];

void gics_init_DAC_vars();
void Update_DAC_Handler();
void gics_update_DAC_vars();



#endif /* GICS_H_ */
