gics_protocol = Proto("G-ICS",  "G-ICS Protocol")

req_code = ProtoField.uint8("gics.req_code", "request", base.HEX)
req_len = ProtoField.uint16("gics.req_len", "len", base.uint16)
gics_protocol.fields = {req_code, req_len }


function format_code(val)
print(val)
		local opcode_name = "Unknown"
			if val==0x11 then opcode_name="Read DigitalAnalog" end
			if val==0x12 then opcode_name="Write DigitalAnalog" end
			if val==0x05 then opcode_name="Read Digital" end
			if val==0x06 then opcode_name="Write Digital" end
			if val==0x09 then opcode_name="Read Analog" end
			if val==a then opcode_name="Write Analog" end
			if val=="FF" then opcode_name="Uninitialized" end
			return opcode_name
		end

local h2b = {
['0']='0000 ', ['1']='0001 ', ['2']='0010 ', ['3']='0011 ',
['4']='0100 ', ['5']='0101 ', ['6']='0110 ', ['7']='0111 ',
['8']='1000 ', ['9']='1001 ', ['A']='1010 ', ['B']='1011 ',
['C']='1100 ', ['D']='1101 ', ['E']='1110 ', ['F']='1111 '
}
function hex2bin(n)
print( n:upper():gsub(".", h2b))
  return n:upper():gsub(".", h2b)
end
function dec2bin(n)
  return hex2bin(string.format("%04x",n))
end


function gics_protocol.dissector(buffer, pinfo, tree)
  length = buffer:len()
  if length == 0 then return end
  if (buffer(1,1):uint()==0xd0 ) then 	pinfo.cols.info:set("Request") elseif buffer(1,1):uint()==0xd1  then pinfo.cols.info:set("Answer") end 

  pinfo.cols.protocol = gics_protocol.name

  local subtree = tree:add(gics_protocol, buffer(), "G-ICS Protocol Data")
  subtree:add_le(req_code, buffer(0,1)):append_text(" (" .. format_code(buffer(0,1):uint()).. ")")
    subtree:add(req_len, buffer(2,2)):append_text(" ( Data Len )")
	if buffer(0,1):uint()==0x12 then 
		local datatree=tree:add(gics_protocol,buffer(4,12),"Analog Inputs to PLC")
		pinfo.cols.info:append(" Write DigitalAnalog")
		for i=1,8 do 
		datatree:add("Analog In"):append_text(i .. ":".. buffer(2+2*i,2):uint())
		end
		local datatree=tree:add(gics_protocol,buffer(20,2),"Digital Inputs to PLC"):append_text(":" .. dec2bin(buffer(20,2):uint()))
		end
	if buffer(0,1):uint()==0x11 then 
	pinfo.cols.info:append(" Read DigitalAnalog")
		if buffer(1,1):uint()==0xd1 then 
		local datatree=tree:add(gics_protocol,buffer(4,12),"Analog Outputs from PLC")
		for i=1,8 do 
		datatree:add("Analog Out"):append_text(i .. ":".. buffer(2+2*i,2):uint())
		end
		local datatree=tree:add(gics_protocol,buffer(20,2),"Digital Outputs from PLC"):append_text(":" .. dec2bin(buffer(20,2):uint()))
		end
		end
		
		
end

local udp_port = DissectorTable.get("udp.port")
udp_port:add(2015, gics_protocol)
