﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using EngineIO;
using System.Xml;
using System.Xml.Serialization;

namespace GICSEngineIO
{
    public class MemoryIO
    {
        public Memory m;
        public string ip = "";
        public int order = 0;
        public double min = -100;
        public double max = 100;

        public MemoryIO(Memory m)
        {
            this.m = m;
        }
        public int Address
        {
            get
            {
                return m.Address;
            }
        }
        public string Name
        {
            get
            {
                return m.Name;
            }
        }
        public string Type
        {
            get
            {
                if (m is MemoryBit)
                    return "Bit";
                else if (m is MemoryByte)
                    return "Byte";
                else if (m is MemoryShort)
                    return "Short";
                else if (m is MemoryInt)
                    return "Int";
                else if (m is MemoryLong)
                    return "Long";
                else if (m is MemoryFloat)
                    return "Float";
                else if (m is MemoryDouble)
                    return "Double";
                else if (m is MemoryString)
                    return "String";
                else if (m is MemoryDateTime)
                    return "Date";
                else
                    return "";
            }
        }
        public string Value
        {
            get
            {
                if (m is MemoryBit)
                    return (m as MemoryBit).Value.ToString();
                else if (m is MemoryByte)
                    return (m as MemoryByte).Value.ToString();
                else if (m is MemoryShort)
                    return (m as MemoryShort).Value.ToString();
                else if (m is MemoryInt)
                    return (m as MemoryInt).Value.ToString();
                else if (m is MemoryLong)
                    return (m as MemoryLong).Value.ToString();
                else if (m is MemoryFloat)
                    return (m as MemoryFloat).Value.ToString();
                else if (m is MemoryDouble)
                    return (m as MemoryDouble).Value.ToString();
                else if (m is MemoryString)
                    return (m as MemoryString).Value.ToString();
                else if (m is MemoryDateTime)
                    return (m as MemoryDateTime).Value.ToString();
                else
                    return "";
            }
        }
        public string IP
        {
            get
            {
                return ip;
            }
        }
        public string Max
        {
            get
            {
                return m is MemoryBit ? "" : max.ToString();
            }
        }
        public string Min
        {
            get
            {
                return m is MemoryBit ? "" : min.ToString();
            }
        }
        public string Order
        {
            get
            {
                return ip != "" && order != 0 ? order.ToString() : "";
            }
        }
        public ushort AsAnalog()
        {
            if (m is MemoryByte)
                return (ushort)(((m as MemoryByte).Value - min) * 1023 / (max - min));
            else if (m is MemoryShort)
                return (ushort)(((m as MemoryShort).Value - min) * 1023 / (max - min));
            else if (m is MemoryInt)
                return (ushort)(((m as MemoryInt).Value - min) * 1023 / (max - min));
            else if (m is MemoryLong)
                return (ushort)(((m as MemoryLong).Value - min) * 1023 / (max - min));
            else if (m is MemoryFloat)
                return (ushort)(((m as MemoryFloat).Value - min) * 1023 / (max - min));
            else if (m is MemoryDouble)
                return (ushort)(((m as MemoryDouble).Value - min) * 1023 / (max - min));
            else
                return 0;
        }
        public bool AsDigital()
        {
            return (m as MemoryBit).Value;
        }
        public bool IsAnalog()
        {
            return (m is MemoryByte) || (m is MemoryShort) || (m is MemoryInt) || (m is MemoryLong) || (m is MemoryFloat) || (m is MemoryDouble);
        }
        public bool IsDigital()
        {
            return m is MemoryBit;
        }
        public void SetDigital(bool digital)
        {
            if (m is MemoryBit)
                (m as MemoryBit).Value = digital;
        }
        public void SetAnalog(ushort analog)
        {
            if (m is MemoryByte)
                (m as MemoryByte).Value = (byte)(min + analog * (max - min) / 4095);
            else if (m is MemoryShort)
                (m as MemoryShort).Value = (short)(min + analog * (max - min) / 4095);
            else if (m is MemoryInt)
                (m as MemoryInt).Value = (int)(min + analog * (max - min) / 4095);
            else if (m is MemoryLong)
                (m as MemoryLong).Value = (long)(min + analog * (max - min) / 4095);
            else if (m is MemoryFloat)
                (m as MemoryFloat).Value = (float)(min + analog * (max - min) / 4095);
            else if (m is MemoryDouble)
                (m as MemoryDouble).Value = (double)(min + analog * (max - min) / 4095);
        }
    }
    public class MemoryIOs
    {
        public List<MemoryIO> inputs = new List<MemoryIO>();
        public List<MemoryIO> outputs = new List<MemoryIO>();
        bool running;
        bool connected;
        long period;
        long ticks;

        public MemoryIOs()
        {
            new Thread(new ThreadStart(UpdateIOs)).Start();
        }
        public void Apply(Memory memory)
        {
            MemoryIO m = Find(memory.Address, memory.Name, memory.MemoryType);
            if (m == null && memory.Name != "")
                if (memory.MemoryType == MemoryType.Input || memory.MemoryType == MemoryType.Memory)
                    inputs.Add(new MemoryIO(memory));
                else if (memory.MemoryType == MemoryType.Output)
                    outputs.Add(new MemoryIO(memory));
        }
        public void Apply(MemoriesChangedEventArgs value)
        {
            foreach (Memory m in value.MemoriesBit)
                Apply(m);
            foreach (Memory m in value.MemoriesByte)
                Apply(m);
            foreach (Memory m in value.MemoriesShort)
                Apply(m);
            foreach (Memory m in value.MemoriesInt)
                Apply(m);
            foreach (Memory m in value.MemoriesLong)
                Apply(m);
            foreach (Memory m in value.MemoriesFloat)
                Apply(m);
            foreach (Memory m in value.MemoriesDouble)
                Apply(m);
            foreach (Memory m in value.MemoriesString)
                Apply(m);
            foreach (Memory m in value.MemoriesTimeSpan)
                Apply(m);
        }
        public void Connect(double period)
        {
            this.period = (long)Math.Round(period * 10000000);
            ticks = DateTime.Now.Ticks;
            connected = true;
        }
        public void Disconnect()
        {
            connected = false;
        }
        public MemoryIO Find(int address, string name, MemoryType type)
        {
            if (type == MemoryType.Input || type == MemoryType.Memory)
            {
                foreach (MemoryIO m in inputs)
                    if (m.Address == address && m.Name == name && m.m.MemoryType == type)
                        return m;
            }
            else if (type == MemoryType.Output)
            {
                foreach (MemoryIO m in outputs)
                    if (m.Address == address && m.Name == name)
                        return m;
            }
            return null;
        }
        public void Open(string path, out double period)
        {
            period = 0.0;
            XmlDocument xml = new XmlDocument();
            xml.Load(path);
            XmlNode node = xml.SelectSingleNode("EngineIO/Period");
            if (node != null)
                double.TryParse(node.InnerText, out period);
            foreach (XmlNode xn in xml.SelectNodes("EngineIO/Inputs/Input"))
            {
                int address = 0;
                if (xn.Attributes["address"] != null)
                    int.TryParse(xn.Attributes["address"].Value, out address);
                string name = xn.Attributes["name"] != null ? xn.Attributes["name"].Value : "";
                string ip = xn.Attributes["ip"] != null ? xn.Attributes["ip"].Value : "";
                string order = xn.Attributes["order"] != null ? xn.Attributes["order"].Value : "";
                string min = xn.Attributes["min"] != null ? xn.Attributes["min"].Value : "";
                string max = xn.Attributes["max"] != null ? xn.Attributes["max"].Value : "";
                MemoryIO m = Find(address, name, MemoryType.Input);
                if (m == null)
                    Find(address, name, MemoryType.Memory);
                if (m != null)
                {
                    m.ip = ip;
                    if (ip != "")
                        int.TryParse(order, out m.order);
                    if (!(m.m is MemoryBit))
                    {
                        double.TryParse(min, out m.min);
                        double.TryParse(max, out m.max);
                    }
                }
            }
            foreach (XmlNode xn in xml.SelectNodes("EngineIO/Outputs/Output"))
            {
                int address = 0;
                if (xn.Attributes["address"] != null)
                    int.TryParse(xn.Attributes["address"].Value, out address);
                string name = xn.Attributes["name"] != null ? xn.Attributes["name"].Value : "";
                string ip = xn.Attributes["ip"] != null ? xn.Attributes["ip"].Value : "";
                string order = xn.Attributes["order"] != null ? xn.Attributes["order"].Value : "";
                string min = xn.Attributes["min"] != null ? xn.Attributes["min"].Value : "";
                string max = xn.Attributes["max"] != null ? xn.Attributes["max"].Value : "";
                MemoryIO m = Find(address, name, MemoryType.Output);
                if (m != null)
                {
                    m.ip = ip;
                    if (ip != "")
                        int.TryParse(order, out m.order);
                    if (!(m.m is MemoryBit))
                    {
                        double.TryParse(min, out m.min);
                        double.TryParse(max, out m.max);
                    }
                }
            }
        }
        public void Refresh()
        {
            for (int i = 0; i < inputs.Count; i++)
                if (inputs[i].Name == "")
                    inputs.RemoveAt(i--);
            for (int i = 0; i < outputs.Count; i++)
                if (outputs[i].Name == "")
                    outputs.RemoveAt(i--);
        }
        public void Save(string path, double period)
        {
            XmlWriterSettings options = new XmlWriterSettings();
            options.Encoding = new UTF8Encoding(false);
            options.ConformanceLevel = ConformanceLevel.Document;
            options.Indent = true;
            using (XmlWriter xml = XmlWriter.Create(path))
            {
                xml.WriteStartDocument();
                xml.WriteStartElement("EngineIO");
                xml.WriteElementString("Period", period.ToString());
                xml.WriteStartElement("Inputs");
                foreach (MemoryIO m in inputs)
                {
                    xml.WriteStartElement("Input");
                    xml.WriteAttributeString("address", m.Address.ToString());
                    xml.WriteAttributeString("name", m.Name);
                    xml.WriteAttributeString("ip", m.ip);
                    if (m.ip != "")
                        xml.WriteAttributeString("order", m.order.ToString());
                    if (!(m.m is MemoryBit))
                    {
                        xml.WriteAttributeString("min", m.min.ToString());
                        xml.WriteAttributeString("max", m.max.ToString());
                    }
                    xml.WriteEndElement();
                }
                xml.WriteEndElement();
                xml.WriteStartElement("Outputs");
                foreach (MemoryIO m in outputs)
                {
                    xml.WriteStartElement("Output");
                    xml.WriteAttributeString("address", m.Address.ToString());
                    xml.WriteAttributeString("name", m.Name);
                    xml.WriteAttributeString("ip", m.ip);
                    if (m.ip != "")
                        xml.WriteAttributeString("order", m.order.ToString());
                    if (!(m.m is MemoryBit))
                    {
                        xml.WriteAttributeString("min", m.min.ToString());
                        xml.WriteAttributeString("max", m.max.ToString());
                    }
                    xml.WriteEndElement();
                }
                xml.WriteEndElement();
                xml.WriteEndElement();
                xml.WriteEndDocument();
            }
        }
        public void Stop()
        {
            running = false;
        }
        void UpdateGICS()
        {
            List<string> ins = new List<string>();
            Dictionary<string, ushort[]> aout = new Dictionary<string, ushort[]>();
            Dictionary<string, bool[]> dout = new Dictionary<string, bool[]>();
            foreach (MemoryIO m in outputs)
                if (m.ip != "")
                {
                    string ip = m.ip;
                    if (!ins.Contains(ip))
                        ins.Add(ip);
                }
            foreach (MemoryIO m in inputs)
                if (m.ip != "")
                {
                    string ip = m.ip;
                    if (!aout.ContainsKey(ip))
                    {
                        aout.Add(ip, new ushort[8]);
                        for (int i = 0; i < aout[ip].Length; i++)
                            aout[ip][i] = 512;
                    }
                    if (!dout.ContainsKey(ip))
                        dout.Add(ip, new bool[16]);
                    if (m.order > 0)
                        if (m.IsDigital() && m.order <= dout[ip].Length)
                            dout[ip][m.order - 1] = m.AsDigital();
                        else if (m.IsAnalog() && m.order <= aout[ip].Length)
                            aout[ip][m.order - 1] = m.AsAnalog();
                }
            foreach (string ip in aout.Keys)
            {
                GICS.WriteDA(ip, aout[ip].ToArray(), dout[ip].ToArray());
                if (ins.Contains(ip))
                {
                    Thread.Sleep(100);
                    ushort[] analogs;
                    bool[] digitals;
                    GICS.ReadDA(ip, out analogs, out digitals);
                    foreach (MemoryIO m in outputs)
                        if (m.ip == ip && m.order > 0)
                                if (m.IsDigital() && m.order <= digitals.Length)
                                    m.SetDigital(digitals[m.order - 1]);
                                else if (m.IsAnalog() && m.order <= analogs.Length)
                                    m.SetAnalog(analogs[m.order - 1]);
                }
            }
            foreach (string ip in ins)
                if (!aout.ContainsKey(ip))
                {
                    ushort[] analogs;
                    bool[] digitals;
                    GICS.ReadDA(ip, out analogs, out digitals);
                    foreach (MemoryIO m in outputs)
                        if (m.ip == ip && m.order > 0)
                            if (m.IsDigital() && m.order <= digitals.Length)
                                m.SetDigital(digitals[m.order - 1]);
                            else if (m.IsAnalog() && m.order <= analogs.Length)
                                m.SetAnalog(analogs[m.order - 1]);
                }
        }
        void UpdateIOs()
        {
            MemoryMap.Instance.InputsNameChanged += new MemoriesChangedEventHandler(OnInputsNameChanged);
            MemoryMap.Instance.InputsValueChanged += new MemoriesChangedEventHandler(OnInputsValueChanged);
            MemoryMap.Instance.MemoriesNameChanged += new MemoriesChangedEventHandler(OnMemoriesNameChanged);
            MemoryMap.Instance.MemoriesValueChanged += new MemoriesChangedEventHandler(OnMemoriesValueChanged);
            MemoryMap.Instance.OutputsNameChanged += new MemoriesChangedEventHandler(OnOutputsNameChanged);
            MemoryMap.Instance.OutputsValueChanged += new MemoriesChangedEventHandler(OnOutputsValueChanged);
            running = true;
            while (running)
            {
                MemoryMap.Instance.Update();
                if (connected && ticks < DateTime.Now.Ticks)
                {
                    UpdateGICS();
                    ticks += period;
                }
                Thread.Sleep(10);
            }
            MemoryMap.Instance.Dispose();
        }
        void OnInputsNameChanged(MemoryMap sender, MemoriesChangedEventArgs value)
        {
            Apply(value);
        }
        void OnInputsValueChanged(MemoryMap sender, MemoriesChangedEventArgs value)
        {
            Apply(value);
        }
        void OnMemoriesNameChanged(MemoryMap sender, MemoriesChangedEventArgs value)
        {
            Apply(value);
        }
        void OnMemoriesValueChanged(MemoryMap sender, MemoriesChangedEventArgs value)
        {
            Apply(value);
        }
        void OnOutputsNameChanged(MemoryMap sender, MemoriesChangedEventArgs value)
        {
            Apply(value);
        }
        void OnOutputsValueChanged(MemoryMap sender, MemoriesChangedEventArgs value)
        {
            Apply(value);
        }
    }
}
